# Wiki

> This repository has been archived. All information has been migrated to our website, https://opensavvy.dev.

This repository contains a collection of conventions for OpenSavvy projects. 
We encourage you to use them in your own projects, too.

Most of the time, we attempt to keep our conventions as close as possible to what is idiomatic—for example, our Kotlin coding style is almost identical to the official one written by JetBrains, the only differences are additions to things that weren't mentioned, or decisions when the official style gives a choice.

## Learn more about us

- [Who are we?](opensavvy.md)

## Learn how to contribute

Contributing to our projects:

- Before you start: [Forks and why we use them](tutorials/forks.md)
- [Making changes and submitting them](tutorials/changes.md)
- [How to have a successful code review](tutorials/review.md)

Other useful information:

- You reached the GitLab CI quota: [Configure your own runner](tooling/gitlab/runner-setup.md)
- You want to configure your project in the same way we do: [Configure a GitLab project](tooling/gitlab/setup.md)
- You want a project template that follows our rules: [Discover the Playground](https://gitlab.com/opensavvy/playgrounds)

Go more in-depth:

- How we organize our branches: the [OpenSavvy Flow](tooling/git/flow.md)
- How we structure commits: the [OpenSavvy Commit Style](tooling/git/commits.md)

## Learn more about the rules we follow

- [Open Source licenses we approve](licensing/README.md)
- [How we classify the stability of our projects](./stability.md)
- [Our coding style](coding-style)
  - [Common to all languages](coding-style/README.md)
  - [Kotlin-specific](coding-style/kotlin.md)

## Following these rules in your project

We recommend to add a "Contributing" section in your README, or in a CONTRIBUTING file, which links to this repository and states which parts of our conventions you are following.

## License

This project is licensed under Apache 2.0, see the full text in the [LICENSE.txt file](LICENSE.txt).
