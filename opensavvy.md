# OpenSavvy

OpenSavvy is a group of developers with the aim of creating tools _we are proud of_.

|                         Ivan “CLOVIS” Canet • [@clovis-ai](https://gitlab.com/clovis-ai)                         |                        Maxime Girardet • [@maximegirardet](https://gitlab.com/maximegirardet)                         |
|:----------------------------------------------------------------------------------------------------------------:|:---------------------------------------------------------------------------------------------------------------------:|
| ![](https://gitlab.com/uploads/-/system/user/avatar/2425545/avatar.png?width=400 "Ivan Canet's profile picture") | ![](https://gitlab.com/uploads/-/system/user/avatar/7833560/avatar.png?width=400 "Maxime Girardet's profile picture") |

OpenSavvy has multiple core values:

- **Open Source** — Code is trustworthy because it is world-readable
- **Free** — Running our projects on your own hardware is free
- **Security** — Users retain full control over their data, with full transparency over security threats
- **Quality** — A tool that doesn't work well is of use to no one
- **Automation** — The only way to move fast without sacrificing any of the above is to automate _everything_
