# Licensing

> The contents of this file are guidelines and rationales for OpenSavvy projects. 
> They have no legal value. 
> Always refer to the full license text for the specific project for legal information. 

All projects should have a mention of which license they are licensed under in their `README.md` file. The mention should link to the full text of the license, which should be distributed as part of the project to ensure no confusion in regard to license versioning (e.g. in a file called `LICENSE.txt`).

## Apache 2.0

> [License text](LICENSE-APACHE-2.0.txt)

We recommend using Apache 2.0 for projects primarily aimed for building up on, instead of being aimed for end users. For example, we recommend Apache 2.0 for libraries or internal tooling.

We want to encourage our libraries to be used in any place they can be useful, even if as part of a commercial product.

## GNU Affero General Public License, version 3

> [License text](LICENSE-AGPL-3.0.txt)

We recommend using AGPL 3.0 for projects aimed primarily for end users.

We want to encourage our products to be used in any place they can be useful, as long as improvements made by anyone are available to us and everyone else using them.
