# About 

This guide aims to provide a reliable process to set up **local runners** on your personal fork of OpenSavvy projects.

# Setting up GitLab runners

There are two ways to use GitLab runners for CI: **shared runners**, hosted on GitLab servers, and **local runners**, which live on your machine.

If you elect to use shared runners, this guide will not be necessary. However, if your GitLab account was created after May 17, 2021, you will need to provide a credit card number, at no cost ([learn more](https://about.GitLab.com/blog/2021/05/17/prevent-crypto-mining-abuse/)).

The rest of this tutorial describes the setup process for local runners.

## General runner profiles 

OpenSavvy projects typically use two runners: 

* A _privileged_ (docker-in-docker) runner that will handle build jobs by creating Docker images 
* A _general-purpose_ runner, charged with executing other jobs.

Jobs aimed at the privileged runner will be tagged with the "docker" label. Other jobs are generally left untagged.

## Installing GitLab runners locally

Please refer to [this page](https://docs.gitlab.com/runner/install/) to find the installation process for GitLab runners on your system.

## Runner setup

Note that the conventions described below match those used for shared runners, thus ensuring compatibility with them.

### General-purpose runner

In the drop-down menu found on the left of your fork, navigate to **Settings > CI/CD**.
On the following page, click the *Expand* button of the *Runners* section.

Using the *New project runner* button, create a runner with the *Run untagged jobs* checkbox of the *Runners* section checked, with its description set to "general-purpose".
After the runner is created, copy and paste the provided **registration token**.

In your local terminal, run the following command:

```
sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token REGISTRATION_TOKEN \
  --executor docker \
  --description "general-purpose" \
  --docker-image "docker:20.10.16" 
```

substituting the appropriate REGISTRATION_TOKEN, to register the general-purpose runner on your system.

### Privileged runner

In the drop-down menu found on the left of your fork, navigate to **Settings > CI/CD**.
On the following page, click the *Expand* button of the *Runners* section.

Using the *New project runner* button, create a runner with the "docker" tag under the *Tags* list, with its description set to "docker-in-docker".
After the runner is created, copy and paste the provided **registration token**.

In your local terminal, run the following command: 

```
sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token REGISTRATION_TOKEN \
  --executor docker \
  --description "docker-in-docker" \
  --docker-image "docker:20.10.16" \
  --docker-privileged \
  --docker-volumes "/certs/client"
```

substituting the appropriate REGISTRATION_TOKEN, to register the privileged runner on your system.

### Official documentation

For more detailed information, you can refer to the [official GitLab runner registration documentation](https://docs.gitlab.com/runner/register/index.html).

## After setup 

### Activating runners

You can use `systemctl start gitlab-runner` to activate the gitlab-runner service, which will start up the registered runners.

For convenience, it is recommended that you also run `systemctl enable gitlab-runner` once after configuration; this way, your runners will start automatically when booting up your system.

### Job concurrency

In the gitlab-runner `config.toml` file (found by default in `/etc/gitlab-runner/config.toml`), you can change the `concurrent` property to define how many jobs are allowed to run at once.
Higher concurrency lets your pipelines run faster; however, it also adds to resource consumption.

You can pick the value that works best for you, based on your resources and the length of the pipelines you need to run.
